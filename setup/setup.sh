#!/bin/bash
output=$(apt list --installed|grep cunit|wc -l)
if [ "$output" -eq "0" ]; then
    echo "CUNIT will be installed on your system."
    sudo apt-get install libcunit1 libcunit1-doc libcunit1-dev 
fi
