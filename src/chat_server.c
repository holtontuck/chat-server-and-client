//This code is "borrowed" from TCP/IP SOCKETS IN C: Practical Guide For
//Programmers, by Michael J. Donahoo & Kenneth L. Calvert, pp. 19-20,
//Christopher Naugle updated the code to reflect some safer and/or newer C coding practices

#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "net_lib.h"
#include "data_struct.h"

#define MAXPENDING 5


void execute_command(Chat_room* Rooms, struct sockaddr_in client_addr, char* payload);
void send_message_in_channel(struct sockaddr_in client_addr, char* payload);

#define MAX_ROOMS 100


ssize_t chat_room_num = -1;


int HandleTCPClient(int clnt_socket, struct sockaddr_in client_addr)
{
    Chat_room* Rooms = calloc(MAX_ROOMS, sizeof(*Rooms));
    if (Rooms == NULL)
    {
        fprintf(stderr, "Couldn't allocate memory for the chat rooms!\n");
        exit(1);
    }

    char client_addr_string[18] = {0};
    printf("Client addr %s:%d\n", inet_ntop(AF_INET,
            &client_addr.sin_addr, client_addr_string,
            sizeof(client_addr_string)), ntohs(client_addr.sin_port));

    while (1)
    {
        char *echo_buffer = calloc(1, MTU);
        if (echo_buffer == NULL)
        {
            fprintf(stderr, "Couldn't allocate memory for command line buffer.\n");
            return 1;
        }
        int recv_msg_size;
        //Receive the message from the client
        if ((recv_msg_size = recv(clnt_socket, echo_buffer, MTU, 0)) < 0)
        {
            fprintf(stderr, "Error receiving client message!\n");
            return 1;
        }
        //Return the received string back to the client
            if (Send(clnt_socket, echo_buffer, recv_msg_size, 0) != recv_msg_size)
            {
                fprintf(stderr, "Mismatched bytes. Didn't Send the correct number\
                    of bytes!\n");
                Close(clnt_socket);
                return 1;
            }
        char type = 0;
        type -= '0';
        char payload[530] = {0};

        memcpy(&type, echo_buffer, sizeof(type));
        memcpy(payload, echo_buffer+ sizeof(type), strlen(echo_buffer) - 1);
        //Convert value from ascii to an integer representation
        type -= '0';
        switch(type)
        {
            case(command):
                printf("type is command\n");
                execute_command(Rooms, client_addr, payload);
                break;
            case(message):
                send_message_in_channel(client_addr, payload);
                break;
            case(quit):
                destroy_list(Rooms, chat_room_num + 1);
                free(echo_buffer);
                exit(0);
            default:
                printf("Unknown type\n");
                return 1;
        }
        free(echo_buffer);
    }
    return 0;
}


bool join_room(Chat_room* Rooms, char* user, struct sockaddr_in client_addr, char room_id[MAX_FIELD_WIDTH])
{
    bool occupant_added = false;
    //Loop through all the chat rooms
    for (ssize_t room = 0; room <= chat_room_num; ++room)
    {
        //Looking for the request chat room to join
        if (strncmp(room_id, Rooms[room].room_id, MAX_FIELD_WIDTH) == 0)
        {
            //If room is found, it doesn't need to be created
            printf("Room Exists\n");
            Node* curr = Rooms[room].occupant_list->head;
            //Check to see if the requested user is already in the room
            //Cycle through the linked list of the room's occupants
            while (curr)
            {
                if (strncmp(user, curr->occupant, MAX_FIELD_WIDTH) == 0)
                {
                    printf("Already in room\n");
                    return true;
                }
                curr = curr->next;
            }
            printf("Adding %s\n", user);
            //Add the user to the list of the room's occupants
            add_node_at_head(Rooms[room].occupant_list, user, client_addr);
            occupant_added = true;
            return true;
        }
    }
    printf("Adding chat room, \'%s\'\n", room_id);
    ++chat_room_num;
    Rooms[chat_room_num].occupant_list = 
        calloc(1, sizeof(*Rooms[chat_room_num].occupant_list));
    if (Rooms[chat_room_num].occupant_list == NULL)
    {
    fprintf(stderr, "Couldn't allocate memory for list of room occupants\n");
        return false;
    }
    Rooms[chat_room_num].occupant_list->head = NULL;
    memcpy(Rooms[chat_room_num].room_id, room_id, MAX_FIELD_WIDTH);
    printf("Adding %s\n", user);
    add_node_at_head(Rooms[chat_room_num].occupant_list, user, client_addr);
    return true;
}


void execute_command(Chat_room* Rooms, struct sockaddr_in client_addr, char* incoming_payload)
{
    bool success = false;
    char type = 0;
    char payload[50] = {0};
    //Subract one to account for the stripped first byte (type)
    size_t  payload_len = strlen(incoming_payload) - 1;
    //Copy the first byte into 'type'
    memcpy(&type, incoming_payload, sizeof(type));
    //strip the type off the incoming payload as it is copied 
    memcpy(payload, incoming_payload + sizeof(type), payload_len);

    //Convert ascii to integer representation
    type -= '0';
    if (type == _register)
    {
        //TODO Put the register_command on the heap with calloc
        printf("Command: register\n");
        Register register_command = {0};
        memcpy(&register_command.user, payload, MAX_FIELD_WIDTH);
        memcpy(&register_command.pass, payload + MAX_FIELD_WIDTH,
            MAX_FIELD_WIDTH);
        printf("USER: %s\n", register_command.user);
        printf("PASSWORD: %s\n", register_command.pass);
    }
    else if (type == join)
    {
        printf("Command: join\n");
        Join *join_command = calloc(1, sizeof(*join_command));
        if (join_command == NULL)
        {
            fprintf(stderr, "Couldn't allocate memory\n");
            return;
        }
        memcpy(join_command->room_id, payload, MAX_FIELD_WIDTH - 1);
        join_command->room_id[MAX_FIELD_WIDTH - 1] = '\0';
        memcpy(join_command->user, payload + MAX_FIELD_WIDTH, MAX_FIELD_WIDTH - 1);
        join_command->user[MAX_FIELD_WIDTH - 1] = '\0';
        success = join_room(Rooms, join_command->user, client_addr, join_command->room_id);
        free(join_command);
        for (ssize_t room = 0; room <= chat_room_num; ++room)
        {
            printf("OCCUPANTS:\n");
            print_list(Rooms[room].occupant_list);
            printf("********************************\n");
        }
    }
    else
    {
        printf("Unknown command\n");
        return;
    }
    return;
} 


void send_message_in_channel(struct sockaddr_in client_addr, char* payload)
{
}


int
main(void)
{
    
    int servSock = 0;
    int clnt_socket = 0;
    struct sockaddr_in echoServAddr = {0};
    struct sockaddr_in echoClntAddr = {0};
    unsigned short echoServPort = 0;
    unsigned int clntLen = 0;

    echoServPort = 6667;
    
    if ((servSock = Socket(IPV4, TCP, false)) < 0)
    {
        fprintf(stderr, "Couldn't create socket!\n");
        return 1;
    }   
    //set socket option to reuse sockaddr
    int opt_val = 1;
    setsockopt(servSock, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof(opt_val));

    //zero out the echoServAddr struct
    memset(&echoServAddr, 0, sizeof(echoServAddr));
    echoServAddr.sin_family = AF_INET;
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    echoServAddr.sin_port = htons(echoServPort);

    //Bind to the socket
    if (bind(servSock, (struct sockaddr *) &echoServAddr, 
            sizeof(echoServAddr)) < 0)
    {
        fprintf(stderr, "Couldn't bind to socket!\n");
        Close(clnt_socket);
        return 1;
    }

    //Set socket up to listen for incoming connections
    if (listen(servSock, MAXPENDING) < 0)
    {
        fprintf(stderr, "Couldn't listen on socket!\n");
        Close(clnt_socket);
        return 1;
    }

    for (;;)
    {
        clntLen = sizeof(echoClntAddr);
        //wait for connection request from client
        if ((clnt_socket = accept(servSock, (struct sockaddr *) &echoClntAddr,
                &clntLen)) < 0)
        {
            fprintf(stderr, "Couldn't accept connection request from\
                    client!\n");
            Close(clnt_socket);
            return 1;
        }
        char clnt_addr_string[18]; 
        printf("Handling Client %s\n", inet_ntop(AF_INET,
            &echoClntAddr.sin_addr, clnt_addr_string, sizeof(clnt_addr_string)));
        printf("Returned from handling client!\n");
        int status = 0;
        status = HandleTCPClient(clnt_socket, echoClntAddr);
        if (status == 1)
        {
            Close(clnt_socket);
            return status;
        }
    }
}
