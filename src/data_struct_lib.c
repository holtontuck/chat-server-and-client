#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/socket.h>

#include "data_struct.h"


bool add_node_at_head(l_list* list, char occupant[MAX_FIELD_WIDTH],
    struct sockaddr_in occupant_addr)
{
    //Allocate memory for new node
    Node *new = calloc(1, sizeof(*new));
    if (new == NULL)
    {
        fprintf(stderr, "Calloc Error!\n");
        return 0;
    }
    if (list->head == NULL)
    {
        list->tail = new;
    }
    //put the addr in the new node
    memcpy(new->occupant, occupant, MAX_FIELD_WIDTH);
    new->occupant_addr = occupant_addr;
    new->next = list->head;
    if (list->head != NULL)
    {
        list->head->prev = new;
    }
    list->head = new;
    return true;
}


size_t count_nodes(l_list* list)
{
    Node* curr = list->head;
    if (list == NULL)
    {
        fprintf(stderr, "NULL ptr\n");
        return 0;
    }
    size_t num_entries = 0;
    while (curr)
    {
        ++num_entries;
        curr = curr->next;
    }
    return num_entries;
}


size_t print_reverse(l_list *list)
{
    //Print from tail to head
    size_t count = 0; 
    Node *cur = list->tail;
    while (cur)
    {
        ++count;
        printf("%s\n", cur->occupant);
        cur = cur->prev;
    }
    return count;
}


size_t print_list(l_list *list)
{
    //Print from head to tail
    size_t count = 0; 
    Node *cur = list->head;
    while (cur)
    {
        ++count;
        printf("%s\n", cur->occupant);
        cur = cur->next;
    }
    return count;
}

bool destroy_list(Chat_room* Rooms, size_t num_of_rooms)
{
    Node *tmp;
    //TODO check to make sure input to function is valid
    for (size_t room = 0; room < num_of_rooms; ++room)
    {
        Node *cur = Rooms[room].occupant_list->head;
        l_list* cur_list = Rooms[room].occupant_list;
        //TODO change '100' above to MAX_ROOMS, and move MAX_ROOMS to header file
        while (cur)
        {
            tmp = cur->next;
            free(cur);
            cur = tmp;
        }
        free(cur_list);
    }
    free(Rooms);
    return true;
    
}
