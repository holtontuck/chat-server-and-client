#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/socket.h>
#include <unistd.h>

#include "net_lib.h"

unsigned int sockaddr_in_len = sizeof(struct sockaddr_in);
unsigned int sockaddr_in6_len = sizeof(struct sockaddr_in6);



int
Recv(int sockfd, void *buf, size_t len, int flags)
{
    //This variable will hold the return value from the call to recvfrom()
    int sys_err = 0;
    //This struct will hold the address of the whoever is returning the
    //sequence number
    //This is the size of the fromAddr;
    //Make sure the input parameters are valid; if not return an error code
    if (sockfd < 1)
    { 
        fprintf(stderr, "Invalid socket descriptor!\n");
        return INVALID_DESCRIPTOR;
    }
    if (buf == NULL)
    {
        fprintf(stderr, "NULL POINTER\n");
        return NULL_PTR;   
    }
    if (len < 1)
    {
        fprintf(stderr, "INVALID BUFFER LENGTH\n");
        return INVALID_BUFFER_LENGTH;
    }
    sys_err = recv(sockfd, buf, len, flags);
    return sys_err;
}


int 
Recv_from(int sockfd, void *buf, size_t len, int flags,
    struct sockaddr *src_addr, socklen_t *addr_len)
{
    //This variable will hold the return value from the call to recvfrom()
    int sys_err = 0;
    //This struct will hold the address of the whoever is returning the
    //sequence number
    //This is the size of the fromAddr;
    //Make sure the input parameters are valid; if not return an error code
    if (sockfd < 1)
    { 
        fprintf(stderr, "Invalid socket descriptor!\n");
        return INVALID_DESCRIPTOR;
    }
    
    if (buf == NULL)
    {
        fprintf(stderr, "NULL POINTER\n");
        return NULL_PTR;   
    }
    if (len < 1)
    {
        fprintf(stderr, "INVALID BUFFER LENGTH\n");
        return INVALID_BUFFER_LENGTH;
    }
    if (src_addr == NULL)
    {
        fprintf(stderr, "INVALID SOURCE ADDRESS\n");
        return NULL_PTR;   
    }
    if (*addr_len != sockaddr_in_len && *addr_len != sockaddr_in6_len)
    {
        fprintf(stderr, "INVALID DESTINATION ADDRESS LENGTH\n");
        return NULL_PTR;   
    }
    sys_err = recvfrom(sockfd, buf, len, flags, src_addr, addr_len);
    return sys_err;
}


int 
Send(int sockfd, const void *buf, size_t len, int flags)
{
    int sys_err = 0;
    //Make sure the input parameters are valid; if not return an error code
    if (sockfd < 1)
    { 
        fprintf(stderr, "Invalid socket descriptor!\n");
        return INVALID_DESCRIPTOR;
    }
    if (buf == NULL)
    {
        fprintf(stderr, "NULL POINTER!\n");
        return NULL_PTR;   
    }
    if (len < 1)
    {
        fprintf(stderr, "INVALID BUFFER LENGTH\n");
        return INVALID_BUFFER_LENGTH;
    }
    sys_err = send(sockfd, buf, len, flags);
    return sys_err;
}


int 
Send_to(int sockfd, const void *buf, size_t len, int flags,
    const struct sockaddr *dest_addr, socklen_t addr_len)
{
    int sys_err = 0;
    //Make sure the input parameters are valid; if not return an error code
    if (sockfd < 1)
    { 
        fprintf(stderr, "Invalid socket descriptor!\n");
        return INVALID_DESCRIPTOR;
    }
    if (buf == NULL)
    {
        fprintf(stderr, "NULL POINTER!\n");
        return NULL_PTR;   
    }
    if (len < 1)
    {
        fprintf(stderr, "INVALID BUFFER LENGTH\n");
        return INVALID_BUFFER_LENGTH;
    }
    if (dest_addr == NULL)
    {
        fprintf(stderr, "INVALID DESTINATION ADDRESS!\n");
        return NULL_PTR;   
    }
    if (addr_len != sockaddr_in_len && addr_len != sockaddr_in6_len)
    {
        fprintf(stderr, "INVALID DESTINATION ADDRESS LENGTH!\n");
        return NULL_PTR;   
    }
    sys_err = sendto(sockfd, buf, len, flags, dest_addr, addr_len);
    return sys_err;
}


int
Socket(int family, int proto, bool non_block)
{
    //variables to hold numerical values for socket input parameters
    int type = 0;
    int sock = 0;
    size_t option = 0;
    // Determine how to set the socket
    if ((family != AF_INET) && (family != AF_INET6))
    {
        errno = EAFNOSUPPORT;
        return -1;
    }

    if ((proto != UDP) && (proto != TCP))
    {
        errno = ENOPROTOOPT;
        return -1;
    }
    else if (proto == UDP)
    {
        type = SOCK_DGRAM;
    }
    else
    {
        type = SOCK_STREAM;
    }
    if (non_block)
    {
        option = O_NONBLOCK;
    }
    
    sock = socket(family, type | option, 0);
    if (sock < 1)
    {
        fprintf(stderr, "Socket failure!\n");
    }
    return sock;
}

void Close(int socket)
{
	if (socket < 1)
	{
		errno = EINVAL;
        return;
    }
	close(socket);
}
