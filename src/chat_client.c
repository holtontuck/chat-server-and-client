//This code is "borrowed" from TCP/IP SOCKETS IN C: A Practical Guide For
//Programmers, by Michael J. Donahoo & Kenneth L. Calvert. 
//The code is found on pp. 13-14, and has been modified and updated by
//Christopher 

#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>

#include "net_lib.h"


int
main (int argc, char *argv[])
{
    int sock;
    struct sockaddr_in echoServAddr = {0};
    //The above two structs hold ip addresses for 
    unsigned short echoServPort = 6667;
    char *servIP = NULL;
    char command_string[512] = {0};
    char echoBuffer[MTU];
    memset(echoBuffer,0, sizeof(echoBuffer));
    int command_string_len = 0;
    int bytesRcvd = 0, totalBytesRcvd = 0;
    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s <SERVER IP>\n",
            argv[0]);
        return 1;
    }

    servIP = argv[1];
    if ((sock = Socket(ipv4, TCP, false)) < 0)
    {
        printf("Socket Descriptor: %d\n", sock);
        fprintf(stderr, "Couldn't create socket!\n");
        return 1;
    }
    memset(&echoServAddr, 0, sizeof(echoServAddr));
    //Clear the echoServAddr with zeros
    echoServAddr.sin_family = AF_INET;
    int retVal = 0;
    retVal = inet_pton(AF_INET, servIP, &echoServAddr.sin_addr.s_addr);
    if (retVal != 1)
    {
        fprintf(stderr, "Error translating echoserver address!\n");
        Close(sock);
        return retVal;
    }
    echoServAddr.sin_port = htons(echoServPort);

    //Attempt to connect to the server

    int ret_val = 0;
    ret_val = connect(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr));
    if (ret_val < 0)
    {
        fprintf(stderr, "Couldn't connect to Server!\n");
        perror("Error:");
        Close(sock);
        return -1;
    }
    printf("Connected:\n\n");
    while(1)
    {
    printf(">> ");
    if (fgets(command_string, sizeof(command_string), stdin) == NULL)
    {
        exit(0);
    }
        command_string_len = strlen(command_string);
        
        if (Send(sock, command_string, command_string_len, 0) != command_string_len)
        {
            fprintf(stderr, "Didn't Send correct number of bytes!\n");
            Close(sock);
            return -1;
        }

        printf("Received: ");
        while (totalBytesRcvd < command_string_len)
        {
            //Use the while statement to keep reading in until there is nothing
            //left to receive
            if ((bytesRcvd = Recv(sock, echoBuffer, MTU - 1, 0)) <= 0)
            {
                fprintf(stderr, "Something went wrong with receive!\n");
                Close(sock);
                return -1;
            }
            totalBytesRcvd += bytesRcvd;
            echoBuffer[bytesRcvd] = '\0';
            printf("%s", echoBuffer);
        }
    }
    printf("\n");
    Close(sock);
    return 0;
}
