CFLAGS += -Wall -Wextra -Wpedantic -Wfloat-equal -Wwrite-strings -Wvla -Winline
CFLAGS += -D_DEFAULT_SOURCE 
CFLAGS += -std=gnu11 -Waggregate-return
CFLAGS += -Iinclude
LD_FLAGS +=-L.
LD_LIBS += -lcunit

.PHONY:clean debug profile check


all: src/chat_server src/chat_client
src/chat_server: src/chat_server.c src/net_lib.c src/data_struct_lib.c
src/chat_client: src/chat_client.c src/net_lib.c
all:
	mv src/chat_server src/chat_client .

test: test/net_lib_test
test/Cunit_example: LD_FLAGS+=-L.
test/Cunit_example: LD_LIBS+=-lcunit
test/net_lib_test: test/net_lib_test.c src/net_lib.c
	$(CC) $(CFLAGS) $(LDFLAGS) test/net_lib_test.c src/net_lib.c -lcunit -L. -o test/net_lib_test; mv test/net_lib_test .; reset; ./net_lib_test | tee test/test.out; rm net_lib_test;

valgrind: test/net_lib_test_valgrind
test/Cunit_example: LD_FLAGS+=-L.
test/Cunit_example: LD_LIBS+=-lcunit
test/net_lib_test_valgrind: test/net_lib_test.c src/net_lib.c
	$(CC) $(CFLAGS) $(LDFLAGS) test/net_lib_test.c src/net_lib.c -lcunit -L. -o test/net_lib_test_valgrind; reset; mv test/net_lib_test_valgrind .;valgrind --leak-check=full -v --log-fd=1 ./net_lib_test_valgrind > test/test_valgrind.out; cat test/test_valgrind.out; rm net_lib_test_valgrind;

debug: src/chat_server src/chat_client 
debug: CFLAGS+= -g
debug:
	mv src/chat_server src/chat_client . 

setup:
	setup/setup.sh

clean:
	$(RM) chat_server chat_client net_lib_test*.o *.log src/libnet_lib.so

net_lib:
	gcc -c -fPIC $(CFLAGS) src/net_lib.c -o src/net_lib.o; gcc src/net_lib.o -shared -o libnet_lib.so
	$(RM) src/net_lib.o; mv libnet_lib.so src

doc:
	 rm net_lib--user_guide.pdf; doxygen net_lib_config; cd doxygen/latex ; pdflatex refman.tex; mv refman.pdf ../../doc/net_lib--user_guide.pdf; make clean
