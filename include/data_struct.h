#ifndef DATA_STRUCT_H
#define DATA_STRUCT_H

#define MAX_FIELD_WIDTH 17

typedef struct _register
{
    char                type;
    char                user[17];
    char                pass[17];
} Register;


typedef struct join
{
    char                type;
    char                room_id[17];
    char                user[17];
} Join;

typedef struct message
{
    char                room_id[17];
    char                message[512];
    char                user[17];
} Message;

enum payload_type
{
    command = 1,
    message,
    quit,
};

enum command_type
{
    _register = 1,
    join,
};

struct node
{
    char                    occupant[17];
    struct sockaddr_in      occupant_addr;
    struct node             *next;
    struct node             *prev;
};
typedef struct node Node;


struct llist
{
    struct node *head;
    struct node *tail;
};    

typedef struct llist l_list;

typedef struct room
{
    char room_id[17];
    l_list *occupant_list;
} Chat_room;


bool
add_node_at_head(l_list* list, char* occupant,
    struct sockaddr_in occupant_addr);


size_t
count_nodes(l_list * list);


size_t
print_list(l_list *list);


bool
destroy_list(Chat_room* Rooms, size_t num_chat_rooms);

#endif
