#ifndef NET_LIB_H
#define NET_LIB_H

#include <limits.h>
#include <stdbool.h>
#include <sys/types.h>
#include <stdint.h>


#define MTU 1500

enum ERRORS
{
    INVALID_DESCRIPTOR = INT_MIN,
    NULL_PTR,
    INVALID_BUFFER_LENGTH,
    MALLOC_ERROR,
};

//Use these enums with the Socket()
enum SOCKET_PARAMETERS
{
    ipv4 =  AF_INET,
    IPV4 =  AF_INET,
    ipv6 =  AF_INET6,   
    IPV6 =  AF_INET6,
    udp  =  SOCK_DGRAM,   
    UDP  =  SOCK_DGRAM,
    tcp  =  SOCK_STREAM,    
    TCP  =  SOCK_STREAM,
};

/**
 * This is a wrapper for the C standard socket function.
 * @param family - IPV4 or IPV6
 * @param proto - TCP or UDP
 * @param nonblock - set as true to set up a nonblocking socket
 * @return socket descriptor
 */
int
Socket(int family, int proto, bool non_block);

/**
 * This is a wrapper for the C standard send (typically TCP) function.
 * @param sockfd - the socket descriptor for the send
 * @param *buf - a pointer to the buffer containing the data to be sent
 * @param len - buffer size, in bytes
 * @param flags - send flags. go to the manual page for send
 * @return number of bytes sent, or -1 for error (and errno is set)
 */
int
Send(int sockfd, const void *buf, size_t len, int flags);

/**
 * This is a wrapper for the C standard sendto (typically UDP) function.
 * @param sockfd - the socket descriptor for the send
 * @param *buf - a pointer to the buffer containing the data to be sent
 * @param len - buffer size, in bytes
 * @param flags - send flags. go to the manual page for send
 * @param *dest_addr - pointer to a sock_addr structure containing recipient's
 *          network address
 * @param addrlen - length, in bytes, of the dest_addr
 * @return number of bytes sent, or -1 for error (and errno is set)
 */
int 
Send_to(int sockfd, const void *buf, size_t len, int flags,
    const struct sockaddr *dest_addr, socklen_t addrlen);


/**
 * This is a wrapper for the C standard recv (typically TCP) function.
 * @param sockfd - the socket descriptor for the recv
 * @param *buf - a pointer to the buffer where incoming data will be stored
 * @param len - buffer size, in bytes
 * @param flags - recv flags. go to the manual page for recv
 * @return number of bytes received, or -1 for error (and errno is set)
 */
int
Recv(int sockfd, void *buf, size_t len, int flags);


/**
 * This is a wrapper for the C standard recvfrom (typically UDP) function.
 * @param sockfd - the socket descriptor for the recvfrom
 * @param *buf - a pointer to the buffer where incoming data will be stored
 * @param len - buffer size, in bytes
 * @param flags - recvfrom flags. go to the manual page for recvfrom
 * @param *src_addr - pointer to a sock_addr structure containing sender's
 *          network address
 * @param *addrlen - pointer to a variable containing length, in bytes, of the
 *          src_addr
 * @return number of bytes received, or -1 for error (and errno is set)
 */
int 
Recv_from(int sockfd, void *buf, size_t len, int flags,
    struct sockaddr *src_addr, socklen_t *addrlen);


/**
 * This is a wrapper for the C standard close function.
 * @param socket - the socket to close
 */
void Close(int socket);


/**
 * This function XORs each byte of the incoming data, and overwrites it with
 *      the new XOR'd value (encrypt in-place)
 * @param *buf - a pointer to the buffer containing the data to be encrytped
 * @param len - buffer size, in bytes
 */

#endif

