#include <arpa/inet.h>
#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <CUnit/TestDB.h>
#include <stdio.h>
#include <stdlib.h>

#include "net_lib.h"

int init_suite1(void)
{
//This is where you put any initialization tasks that need to be done
return 0;
}

// The suite cleanup function.
int clean_suite1(void)
{
//This is where you put any cleanup tasks (free, close file, etc) 
//that need to be done
    return 0;
}

/* Simple tests which attempt to build various sockets
 */
int test_num = 0;
//Test the Close function with a bad (negative value)
void build_ipv4_udp_socket_bad_close(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int sock = Socket(IPV4, UDP, false);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(-1);
	CU_ASSERT_EQUAL(errno, 22);
    errno = 0;
    Close(sock);
	CU_ASSERT_EQUAL(errno, 0);
}


//Use Uppercase versions of family and proto
void build_ipv4_udp_socket(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(IPV4, UDP, false);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}


void build_ipv6_udp_socket(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(IPV6, UDP, false);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}


void build_ipv4_tcp_socket(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(IPV4, TCP, false);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}


void build_ipv6_tcp_socket(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(IPV6, TCP, false);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}


void build_ipv4_udp_socket_nonblock(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(IPV4, UDP, true);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}


void build_ipv6_udp_socket_nonblock(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(IPV6, UDP, true);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}


void build_ipv4_tcp_socket_nonblock(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(IPV4, TCP, true);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}


void build_ipv6_tcp_socket_nonblock(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(IPV6, TCP, true);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}

//Use lowercase versions of family and proto
void build_ipv4_udp_socket_lower(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(ipv4, udp, false);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}


void build_ipv6_udp_socket_lower(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(ipv6, udp, false);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}


void build_ipv4_tcp_socket_lower(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(ipv4, tcp, false);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}


void build_ipv6_tcp_socket_lower(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
	int sock = Socket(ipv6, tcp, false);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    Close(sock);
}


void build_ipv4_socket_bad_proto_given(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
    int proto = -2;
	int sock = Socket(ipv4, proto, false);
	CU_ASSERT_EQUAL(sock, -1);
}


void build_ipv4_socket_pass_string_as_proto(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
    const char *proto = "Don't try this at home, boys and girls!";
	int sock = Socket(ipv4, *proto, false);
	CU_ASSERT_EQUAL(sock, -1);
}


void build_ipv6_socket_bad_proto_given(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
    int proto = -2;
	int sock = Socket(ipv6, proto, false);
	CU_ASSERT_EQUAL(sock, -1);
}


void build_ipv6_socket_pass_string_as_proto(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
    const char *proto = "Don't try this at home, boys and girls!";
	int sock = Socket(ipv6, *proto, false);
	CU_ASSERT_EQUAL(sock, -1);
}


void build_udp_socket_bad_family_given(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
    //family value is is arbitrarily chosen, not a "real" family value
    int family = -3716;
	int sock = Socket(family, UDP, false);
	CU_ASSERT_EQUAL(sock, -1);
}


void build_tcp_socket_bad_family_given(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
    //family value is is arbitrarily chosen, not a "real" family value
    int family = -3716;
	int sock = Socket(family, TCP, false);
	CU_ASSERT_EQUAL(sock, -1);
}


void send_udp_dgram_ipv4(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
    int bytes_sent = 0;
	int sock = Socket(ipv4, udp, false);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    char test_string[] = "This is a test string.";
    size_t test_string_len = sizeof(test_string);
    struct sockaddr_in echoServAddr = {0};
    unsigned short echoServPort = 7;
    const char *servIP = "127.0.0.1\0";
    echoServAddr.sin_family = AF_INET;
    echoServAddr.sin_port = htons(echoServPort); 
    int ret_val = inet_pton(AF_INET, servIP,
        &echoServAddr.sin_addr.s_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(sock);
        return;
    }

    bytes_sent = Send_to(sock, &test_string, test_string_len, 0, 
            (struct sockaddr *) &echoServAddr, sizeof(echoServAddr));
    Close(sock); 
	CU_ASSERT_EQUAL(bytes_sent, test_string_len);
    return;
}


void send_and_recv_udp_dgram_ipv4(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int send_sock = Socket(ipv4, udp, false);
	int recv_sock = Socket(ipv4, udp, false);
    char *echo_buffer = calloc(1, (100 * sizeof(char)));
    if (echo_buffer == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }
    size_t buf_len = 100;
    char *test_string = calloc(1, strlen("This is a test string.") + 1);
    if (test_string == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        free(echo_buffer);
        Close(send_sock);
        Close(recv_sock);
        return;
    }
        
    memcpy(test_string, "This is a test string.", strlen("This is a test string."));
    size_t test_string_len = strnlen(test_string, 23);
    struct sockaddr_in echoServAddr = {0};
    socklen_t echo_serv_addr_size = sizeof(echoServAddr);
    struct sockaddr_in fromAddr = {0};
    unsigned short echoServPort = 7777;
    socklen_t from_size = sizeof(fromAddr);
    const char *servIP = "127.0.0.1\0";
    const char *fromIP = "127.0.0.1\0";
    echoServAddr.sin_family = AF_INET;
    echoServAddr.sin_port = htons(echoServPort); 
    fromAddr.sin_family = AF_INET;

    int ret_val = inet_pton(AF_INET, servIP,
        &echoServAddr.sin_addr.s_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    ret_val = inet_pton(AF_INET, fromIP,
        &fromAddr.sin_addr.s_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    if (bind(recv_sock, (struct sockaddr *)&echoServAddr,
            echo_serv_addr_size) <0)
    {
        fprintf(stderr, "Bind failed!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    Send_to(send_sock, test_string, test_string_len, 0, 
        (struct sockaddr *)&echoServAddr, echo_serv_addr_size);

    int bytes_received = Recv_from(recv_sock, echo_buffer, buf_len, 0,
            (struct sockaddr *)&fromAddr, &from_size);
    printf("\nReceived %d bytes.\n", bytes_received);
    CU_ASSERT_EQUAL(bytes_received, test_string_len);
    echo_buffer[bytes_received] = '\0';

    printf(":%s\n", test_string);
    printf(":%s\n", echo_buffer);
    CU_ASSERT_EQUAL(strcmp(test_string, echo_buffer), 0);
    Close(send_sock);
    Close(recv_sock);
    free(echo_buffer);
    free(test_string);
    return;
}


void recv_udp_dgram_ipv4_nonblock(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int send_sock = Socket(ipv4, udp, true);
	int recv_sock = Socket(ipv4, udp, true);
    char *echo_buffer = calloc(1, (100 * sizeof(char)));
    if (echo_buffer == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }
    size_t buf_len = 100;
    char *test_string = calloc(1, strlen("This is a test string.") + 1);
    if (test_string == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        free(echo_buffer);
        Close(send_sock);
        Close(recv_sock);
        return;
    }
        
    memcpy(test_string, "This is a test string.", strlen("This is a test string."));
    struct sockaddr_in echoServAddr = {0};
    struct sockaddr_in fromAddr = {0};
    unsigned short echoServPort = 7777;
    socklen_t from_size = sizeof(fromAddr);
    const char *servIP = "127.0.0.1\0";
    const char *fromIP = "127.0.0.1\0";
    echoServAddr.sin_family = AF_INET;
    echoServAddr.sin_port = htons(echoServPort); 
    fromAddr.sin_family = AF_INET;

    int ret_val = inet_pton(AF_INET, servIP,
        &echoServAddr.sin_addr.s_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    ret_val = inet_pton(AF_INET, fromIP,
        &fromAddr.sin_addr.s_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    Recv_from(recv_sock, echo_buffer, buf_len, 0,
            (struct sockaddr *)&fromAddr, &from_size);
    CU_ASSERT_EQUAL(errno, EAGAIN);
    Close(send_sock);
    Close(recv_sock);
    free(echo_buffer);
    free(test_string);
    return;
}


void send_udp_dgram_ipv6(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
    int bytes_sent = 0;
	int sock = Socket(ipv6, udp, false);
	CU_ASSERT_NOT_EQUAL(sock, -1);
    char test_string[] = "This is a test string.";
    size_t test_string_len = sizeof(test_string);
    struct sockaddr_in6 echoServAddr = {0};
    unsigned short echoServPort = 7;
    const char *servIP = "::1";
    echoServAddr.sin6_family = AF_INET6;
    echoServAddr.sin6_port = htons(echoServPort); 
    int ret_val = inet_pton(AF_INET6, servIP,
        &echoServAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(sock);
        return;
    }

    bytes_sent = Send_to(sock, &test_string, test_string_len, 0, 
            (struct sockaddr *) &echoServAddr, sizeof(struct sockaddr_in6));
    Close(sock); 
	CU_ASSERT_EQUAL(bytes_sent, test_string_len);
    return;
}


void send_and_recv_udp_dgram_ipv6(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int send_sock = Socket(ipv6, udp, false);
	int recv_sock = Socket(ipv6, udp, false);
    char *echo_buffer = calloc(1, (100 * sizeof(char)));
    if (echo_buffer == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }
    char *test_string = calloc(1, strlen("This is a test string.") + 1);
    if (test_string == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        free(echo_buffer);
        Close(send_sock);
        Close(recv_sock);
        return;
    }
        
    memcpy(test_string, "This is a test string.", strlen("This is a test string."));
    struct sockaddr_in6 echoServAddr = {0};
    struct sockaddr_in6 fromAddr = {0};
    unsigned short echoServPort = 7777;
    const char *servIP = "::1";
    const char *fromIP = "::1";
    echoServAddr.sin6_family = AF_INET6;
    echoServAddr.sin6_port = htons(echoServPort); 
    fromAddr.sin6_family = AF_INET6;

    int ret_val = inet_pton(AF_INET6, servIP,
        &echoServAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    ret_val = inet_pton(AF_INET6, fromIP,
        &fromAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    socklen_t echo_serv_addr_size = sizeof(echoServAddr);
    if (bind(recv_sock, (struct sockaddr *)&echoServAddr,
            echo_serv_addr_size) <0)
    {
        fprintf(stderr, "Bind failed!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    size_t test_string_len = strnlen(test_string, 23);
    Send_to(send_sock, test_string, test_string_len, 0, 
        (struct sockaddr *)&echoServAddr, echo_serv_addr_size);

    size_t buf_len = 100;
    socklen_t from_size = sizeof(fromAddr);
    int bytes_received = Recv_from(recv_sock, echo_buffer, buf_len, 0,
            (struct sockaddr *)&fromAddr, &from_size);
    printf("\nReceived %d bytes.\n", bytes_received);
    CU_ASSERT_EQUAL(bytes_received, test_string_len);
    echo_buffer[bytes_received] = '\0';

    printf(":%s\n", test_string);
    printf(":%s\n", echo_buffer);
    CU_ASSERT_EQUAL(strcmp(test_string, echo_buffer), 0);
    Close(send_sock);
    Close(recv_sock);
    free(echo_buffer);
    free(test_string);
    return;
}


void recv_udp_dgram_ipv6_nonblock(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int recv_sock = Socket(ipv6, udp, true);
    char *echo_buffer = calloc(1, (100 * sizeof(char)));
    if (echo_buffer == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        Close(recv_sock);
        return;
    }
   
    struct sockaddr_in6 echoServAddr = {0};
    struct sockaddr_in6 fromAddr = {0};
    unsigned short echoServPort = 7777;
    const char *servIP = "::1";
    const char *fromIP = "::1";
    echoServAddr.sin6_family = AF_INET6;
    echoServAddr.sin6_port = htons(echoServPort); 
    fromAddr.sin6_family = AF_INET6;

    int ret_val = inet_pton(AF_INET6, servIP,
        &echoServAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    ret_val = inet_pton(AF_INET6, fromIP,
        &fromAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    socklen_t from_size = sizeof(fromAddr);
    size_t buf_len = 100;
    Recv_from(recv_sock, echo_buffer, buf_len, 0,
            (struct sockaddr *)&fromAddr, &from_size);
    CU_ASSERT_EQUAL(errno, EAGAIN);
    Close(recv_sock);
    free(echo_buffer);
    return;
}


void send_and_recv_tcp_stream_ipv4(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int send_sock = Socket(ipv4, tcp, false);
	int recv_sock = Socket(ipv4, tcp, false);
    char *echo_buffer = calloc(1, (100 * sizeof(char)));
    if (echo_buffer == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }
    size_t buf_len = 100;
    char *test_string = calloc(1, strlen("This is a test string.") + 1);
    if (test_string == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        free(echo_buffer);
        Close(send_sock);
        Close(recv_sock);
        return;
    }
        
    memcpy(test_string, "This is a test string.", strlen("This is a test string."));
    size_t test_string_len = strnlen(test_string, 23);
    struct sockaddr_in echoServAddr = {0};
    socklen_t echo_serv_addr_size = sizeof(echoServAddr);
    struct sockaddr_in fromAddr = {0};
    unsigned short echoServPort = 7777;
    const char *servIP = "127.0.0.1";
    const char *fromIP = "127.0.0.1";
    echoServAddr.sin_family = AF_INET;
    echoServAddr.sin_port = htons(echoServPort); 
    fromAddr.sin_family = AF_INET;

    int ret_val = inet_pton(AF_INET, servIP,
        &echoServAddr.sin_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    ret_val = inet_pton(AF_INET, fromIP,
        &fromAddr.sin_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    if (bind(recv_sock, (struct sockaddr *)&echoServAddr,
            echo_serv_addr_size) < 0)
    {
        fprintf(stderr, "Bind failed!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }
    //Set server up to listen on bound port
    if (listen(recv_sock, 5) < 0)
    {
        fprintf(stderr, "Couldn't listen on socket!\n");
        return;
    }
    //client try to connect to server
    ret_val = connect(send_sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr));                    
    if (ret_val < 0)                                                                                     
    {   
        fprintf(stderr, "Couldn't connect to Server!\n");                                                
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;                                                                                       
    }
    // Server attempt to accept client's connection request
    int new_sock = accept(recv_sock, (struct sockaddr *) &fromAddr,
        &echo_serv_addr_size);
    if (new_sock < 0)
    {
        fprintf(stderr, "Couldn't accept connection request from client!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }
    //Send the test string out to the server
    ret_val = Send(send_sock, test_string, test_string_len, 0); 
    if (ret_val < 0)
    {
        fprintf(stderr, "Send failed!\n");
        return;
    }
    //Server attempt to receive client's sent stream
    int bytes_received = Recv(new_sock, echo_buffer, buf_len, 0);
    printf("\nReceived %d bytes.\n", bytes_received);
    CU_ASSERT_EQUAL(bytes_received, test_string_len);
    echo_buffer[bytes_received] = '\0';

    printf(":%s\n", test_string);
    printf(":%s\n", echo_buffer);
    CU_ASSERT_EQUAL(strcmp(test_string, echo_buffer), 0);
    Close(send_sock);
    Close(new_sock);
    Close(recv_sock);
    free(echo_buffer);
    free(test_string);
    return;
}


void recv_tcp_stream_ipv4_nonblock(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int send_sock = Socket(ipv4, tcp, true);
	int recv_sock = Socket(ipv4, tcp, true);
    struct sockaddr_in echoServAddr = {0};
    socklen_t echo_serv_addr_size = sizeof(echoServAddr);
    struct sockaddr_in fromAddr = {0};
    unsigned short echoServPort = 7777;
    const char *servIP = "127.0.0.1";
    const char *fromIP = "127.0.0.1";
    echoServAddr.sin_family = AF_INET;
    echoServAddr.sin_port = htons(echoServPort); 
    fromAddr.sin_family = AF_INET;

    int ret_val = inet_pton(AF_INET, servIP,
        &echoServAddr.sin_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }

    ret_val = inet_pton(AF_INET, fromIP,
        &fromAddr.sin_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }

    if (bind(recv_sock, (struct sockaddr *)&echoServAddr,
            echo_serv_addr_size) < 0)
    {
        fprintf(stderr, "Bind failed!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }

    //Set server up to listen on bound port
    if (listen(recv_sock, 5) < 0)
    {
        fprintf(stderr, "Couldn't listen on socket!\n");
        return;
    }

    //client try to connect to server
    ret_val = connect(send_sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr));                    
    if (ret_val < 0 && errno != EINPROGRESS)                                                                                     
    {  
        fprintf(stderr, "Couldn't connect to Server!\n");                                                
        Close(send_sock);
        Close(recv_sock);
        return;
    }                                                                                       
    CU_ASSERT_EQUAL(errno, EINPROGRESS);
    Close(send_sock);
    Close(recv_sock);
    return;
}


void recv_tcp_stream_ipv6_nonblock(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int send_sock = Socket(ipv6, tcp, true);
	int recv_sock = Socket(ipv6, tcp, true);
    struct sockaddr_in6 echoServAddr = {0};
    struct sockaddr_in6 fromAddr = {0};
    unsigned short echoServPort = 7777;
    socklen_t echo_serv_addr_size = sizeof(echoServAddr);
    const char *servIP = "::1";
    const char *fromIP = "::1";
    echoServAddr.sin6_family = AF_INET6;
    echoServAddr.sin6_port = htons(echoServPort); 
    fromAddr.sin6_family = AF_INET;

    int ret_val = inet_pton(AF_INET6, servIP,
        &echoServAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }

    ret_val = inet_pton(AF_INET6, fromIP,
        &fromAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }

    if (bind(recv_sock, (struct sockaddr *)&echoServAddr,
        echo_serv_addr_size) < 0)
    {
        fprintf(stderr, "Bind failed!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }

    //Set server up to listen on bound port
    if (listen(recv_sock, 5) < 0)
    {
        fprintf(stderr, "Couldn't listen on socket!\n");
        return;
    }

    //client try to connect to server
    ret_val = connect(send_sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr));                    
    if (ret_val < 0 && errno != EINPROGRESS)                                                                                     
    {  
        fprintf(stderr, "Couldn't connect to Server!\n");                                                
        Close(send_sock);
        Close(recv_sock);
        return;
    }                                                                                       
    CU_ASSERT_EQUAL(errno, EINPROGRESS);
    Close(send_sock);
    Close(recv_sock);
    return;
}


void send_and_recv_tcp_stream_ipv6(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int send_sock = Socket(ipv6, tcp, false);
	int recv_sock = Socket(ipv6, tcp, false);
    char *echo_buffer = calloc(1, (100 * sizeof(char)));
    if (echo_buffer == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }
    size_t buf_len = 100;
    char *test_string = calloc(1, strlen("This is a test string.") + 1);
    if (test_string == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        free(echo_buffer);
        Close(send_sock);
        Close(recv_sock);
        return;
    }
        
    memcpy(test_string, "This is a test string.", strlen("This is a test string."));
    size_t test_string_len = strnlen(test_string, 23);
    struct sockaddr_in6 echoServAddr = {0};
    socklen_t echo_serv_addr_size = sizeof(echoServAddr);
    struct sockaddr_in6 fromAddr = {0};
    unsigned short echoServPort = 7777;
    const char *servIP = "::1";
    const char *fromIP = "::1";
    echoServAddr.sin6_family = AF_INET6;
    echoServAddr.sin6_port = htons(echoServPort); 
    fromAddr.sin6_family = AF_INET6;

    int ret_val = inet_pton(AF_INET6, servIP,
        &echoServAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    ret_val = inet_pton(AF_INET6, fromIP,
        &fromAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }

    if (bind(recv_sock, (struct sockaddr *)&echoServAddr,
            echo_serv_addr_size) <0)
    {
        fprintf(stderr, "Bind failed!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }
    //Set server up to listen on bound port
    if (listen(recv_sock, 5) <0)
    {
        fprintf(stderr, "Couldn't listen on socket!\n");
        return;
    }
    //client try to connect to server
    ret_val = connect(send_sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr));                    
    if (ret_val < 0)                                                                                     
    {   
        fprintf(stderr, "Couldn't connect to Server!\n");                                                
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;                                                                                       
    }
    // Server attempt to accept client's connection request
    int new_sock = accept(recv_sock, (struct sockaddr *) &fromAddr,
        &echo_serv_addr_size);
    if (new_sock < 0)
    {
        fprintf(stderr, "Couldn't accept connection request from client!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        free(test_string);
        return;
    }
    //Send the test string out to the server
    ret_val = Send(send_sock, test_string, test_string_len, 0); 
    if (ret_val < 0)
    {
        fprintf(stderr, "Send failed!\n");
        return;
    }
    //Server attempt to receive client's sent stream
    int bytes_received = Recv(new_sock, echo_buffer, buf_len, 0);
    printf("\nReceived %d bytes.\n", bytes_received);
    CU_ASSERT_EQUAL(bytes_received, test_string_len);
    echo_buffer[bytes_received] = '\0';

    Close(send_sock);
    Close(new_sock);
    Close(recv_sock);
    free(echo_buffer);
    free(test_string);
    return;
}


void test_MTU_limit_ipv6_TCP(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int send_sock = Socket(ipv6, tcp, false);
	int recv_sock = Socket(ipv6, tcp, false);
    char *echo_buffer = calloc(1, (3000 * sizeof(char)));
    if (echo_buffer == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }
    size_t buf_len = 3000;
    char test_string[2200] = {2};        
    size_t test_string_len = sizeof(test_string);
    struct sockaddr_in6 echoServAddr = {0};
    socklen_t echo_serv_addr_size = sizeof(echoServAddr);
    struct sockaddr_in6 fromAddr = {0};
    unsigned short echoServPort = 6789;
    const char *servIP = "::1";
    const char *fromIP = "::1";
    echoServAddr.sin6_family = AF_INET6;
    echoServAddr.sin6_port = htons(echoServPort); 
    fromAddr.sin6_family = AF_INET6;

    int ret_val = inet_pton(AF_INET6, servIP,
        &echoServAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    ret_val = inet_pton(AF_INET6, fromIP,
        &fromAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        //Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    if (bind(recv_sock, (struct sockaddr *)&echoServAddr,
            echo_serv_addr_size) <0)
    {
        fprintf(stderr, "Bind failed!\n");
        Close(recv_sock);
        Close(send_sock);
        free(echo_buffer);
        return;
    }
    //Set server up to listen on bound port
    if (listen(recv_sock, 5) <0)
    {
        fprintf(stderr, "Couldn't listen on socket!\n");
        return;
    }
    //client try to connect to server
    ret_val = connect(send_sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr));                    
    if (ret_val < 0)                                                                                     
    {   
        fprintf(stderr, "Couldn't connect to Server!\n");                                                
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;                                                                                       
    }
    // Server attempt to accept client's connection request
    int new_sock = accept(recv_sock, (struct sockaddr *) &fromAddr,
        &echo_serv_addr_size);
    if (new_sock < 0)
    {
        fprintf(stderr, "Couldn't accept connection request from client!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }
    //Send the test string out to the server
    ret_val = Send(send_sock, test_string, test_string_len, 0); 
    if (ret_val < 0)
    {
        fprintf(stderr, "Send failed!\n");
        return;
    }
    //Server attempt to receive client's sent stream
    int bytes_received = Recv(new_sock, echo_buffer, buf_len, 0);
    printf("\nReceived %d bytes.\n", bytes_received);
    CU_ASSERT_EQUAL(bytes_received, test_string_len);
    echo_buffer[bytes_received] = '\0';
    printf(":%s\n", echo_buffer);
    CU_ASSERT_EQUAL(strncmp(test_string, echo_buffer, strlen(test_string)), 0);
    Close(send_sock);
    Close(recv_sock);
    Close(new_sock);
    free(echo_buffer);
    return;
}


void test_MTU_limit_ipv4_TCP(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int send_sock = Socket(ipv4, tcp, false);
	int recv_sock = Socket(ipv4, tcp, false);
    char *echo_buffer = calloc(1, (3000 * sizeof(char)));
    if (echo_buffer == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }
    size_t buf_len = 3000;
    char test_string[2200] = {2};        
    size_t test_string_len = sizeof(test_string);
    struct sockaddr_in echoServAddr = {0};
    socklen_t echo_serv_addr_size = sizeof(echoServAddr);
    struct sockaddr_in fromAddr = {0};
    unsigned short echoServPort = 6789;
    const char *servIP = "127.0.0.1";
    const char *fromIP = "127.0.0.1";
    echoServAddr.sin_family = AF_INET;
    echoServAddr.sin_port = htons(echoServPort); 
    fromAddr.sin_family = AF_INET;

    int ret_val = inet_pton(AF_INET, servIP,
        &echoServAddr.sin_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    ret_val = inet_pton(AF_INET, fromIP,
        &fromAddr.sin_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        //Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    if (bind(recv_sock, (struct sockaddr *)&echoServAddr,
            echo_serv_addr_size) <0)
    {
        fprintf(stderr, "Bind failed!\n");
        Close(recv_sock);
        Close(send_sock);
        free(echo_buffer);
        return;
    }
    //Set server up to listen on bound port
    if (listen(recv_sock, 5) <0)
    {
        fprintf(stderr, "Couldn't listen on socket!\n");
        return;
    }
    //client try to connect to server
    ret_val = connect(send_sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr));                    
    if (ret_val < 0)                                                                                     
    {   
        fprintf(stderr, "Couldn't connect to Server!\n");                                                
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;                                                                                       
    }
    // Server attempt to accept client's connection request
    int new_sock = accept(recv_sock, (struct sockaddr *) &fromAddr,
        &echo_serv_addr_size);
    if (new_sock < 0)
    {
        fprintf(stderr, "Couldn't accept connection request from client!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }
    //Send the test string out to the server
    ret_val = Send(send_sock, test_string, test_string_len, 0); 
    if (ret_val < 0)
    {
        fprintf(stderr, "Send failed!\n");
        return;
    }
    //Server attempt to receive client's sent stream
    int bytes_received = Recv(new_sock, echo_buffer, buf_len, 0);
    printf("\nReceived %d bytes.\n", bytes_received);
    CU_ASSERT_EQUAL(bytes_received, test_string_len);
    echo_buffer[bytes_received] = '\0';
    printf(":%s\n", echo_buffer);
    CU_ASSERT_EQUAL(strncmp(test_string, echo_buffer, strlen(test_string)), 0);
    Close(send_sock);
    Close(recv_sock);
    Close(new_sock);
    free(echo_buffer);
    return;
}


void test_MTU_limit_ipv6_UDP(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int send_sock = Socket(ipv6, udp, false);
	int recv_sock = Socket(ipv6, udp, false);
    char *echo_buffer = calloc(1, (5001 * sizeof(char)));
    if (echo_buffer == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }
    size_t buf_len = 5000;
    char test_string[5000] = {5};      
    size_t test_string_len = sizeof(test_string);
    struct sockaddr_in6 echoServAddr = {0};
    struct sockaddr_in6 fromAddr = {0};
    unsigned short echoServPort = 7890;
    const char *servIP = "::1";
    const char *fromIP = "::1";
    echoServAddr.sin6_family = AF_INET6;
    echoServAddr.sin6_port = htons(echoServPort); 
    fromAddr.sin6_family = AF_INET6;

    int ret_val = inet_pton(AF_INET6, servIP,
        &echoServAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    ret_val = inet_pton(AF_INET6, fromIP,
        &fromAddr.sin6_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    socklen_t echo_serv_addr_size = sizeof(echoServAddr);
    if (bind(recv_sock, (struct sockaddr *)&echoServAddr,
            echo_serv_addr_size) <0)
    {
        fprintf(stderr, "Bind failed!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    Send_to(send_sock, test_string, test_string_len, 0, 
        (struct sockaddr *)&echoServAddr, echo_serv_addr_size);

    socklen_t from_size = sizeof(fromAddr);
    int bytes_received = Recv_from(recv_sock, echo_buffer, buf_len, 0,
            (struct sockaddr *)&fromAddr, &from_size);
    printf("\nReceived %d bytes.\n", bytes_received);
    CU_ASSERT_EQUAL(bytes_received, test_string_len);
    echo_buffer[bytes_received] = '\0';
    printf(":%s\n", echo_buffer);
    CU_ASSERT_EQUAL(strcmp(test_string, echo_buffer), 0);
    Close(send_sock);
    Close(recv_sock);
    free(echo_buffer);
    return;
}


void test_MTU_limit_ipv4_UDP(void)
{
    ++test_num;
    printf("Test #%d\n", test_num);
    errno = 0;
	int send_sock = Socket(ipv4, udp, false);
	int recv_sock = Socket(ipv4, udp, false);
    char *echo_buffer = calloc(1, (5001 * sizeof(char)));
    if (echo_buffer == NULL)
    {
        fprintf(stderr, "Memory Allocation Error!\n");
        Close(send_sock);
        Close(recv_sock);
        return;
    }
    size_t buf_len = 5000;
    char test_string[5000] = {5};      
    size_t test_string_len = sizeof(test_string);
    struct sockaddr_in echoServAddr = {0};
    struct sockaddr_in fromAddr = {0};
    unsigned short echoServPort = 7890;
    const char *servIP = "127.0.0.1";
    const char *fromIP = "127.0.0.1";
    echoServAddr.sin_family = AF_INET;
    echoServAddr.sin_port = htons(echoServPort); 
    fromAddr.sin_family = AF_INET;

    int ret_val = inet_pton(AF_INET, servIP,
        &echoServAddr.sin_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    ret_val = inet_pton(AF_INET, fromIP,
        &fromAddr.sin_addr);
    if (ret_val != 1)
    {
        fprintf(stderr, "Failed to convert ip string to network number!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    socklen_t echo_serv_addr_size = sizeof(echoServAddr);
    if (bind(recv_sock, (struct sockaddr *)&echoServAddr,
            echo_serv_addr_size) <0)
    {
        fprintf(stderr, "Bind failed!\n");
        Close(send_sock);
        Close(recv_sock);
        free(echo_buffer);
        return;
    }

    Send_to(send_sock, test_string, test_string_len, 0, 
        (struct sockaddr *)&echoServAddr, echo_serv_addr_size);

    socklen_t from_size = sizeof(fromAddr);
    int bytes_received = Recv_from(recv_sock, echo_buffer, buf_len, 0,
            (struct sockaddr *)&fromAddr, &from_size);
    printf("\nReceived %d bytes.\n", bytes_received);
    CU_ASSERT_EQUAL(bytes_received, test_string_len);
    echo_buffer[bytes_received] = '\0';
    printf(":%s\n", echo_buffer);
    CU_ASSERT_EQUAL(strcmp(test_string, echo_buffer), 0);
    Close(send_sock);
    Close(recv_sock);
    free(echo_buffer);
    return;
}

/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another
 * CUnit error code on failure.
 */
int main()
{
    CU_pSuite pSuite = NULL;

    /* initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry())
      return CU_get_error();

    /* add a suite to the registry */
    pSuite = CU_add_suite("Suite_1", NULL, NULL);
    if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
    }

    /* add the tests to the suite */
    // Test passing a negative socket descriptor to Close()
    if (CU_add_test(pSuite, "test of ipv4 UDP socket, passing a negative number\
                to close the socket", build_ipv4_udp_socket_bad_close) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Uppercase versions of family, proto
    if (CU_add_test(pSuite, "test of ipv4 UDP socket_use_uppercase",
            build_ipv4_udp_socket) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test of ipv6 UDP socket_use_uppercase",
            build_ipv6_udp_socket) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test of ipv4 TCP socket_use_uppercase",
            build_ipv4_tcp_socket) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test of ipv6 TCP socket_use_uppercase",
            build_ipv6_tcp_socket) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Lowercase versions of family, proto
    if (CU_add_test(pSuite, "test of ipv4 UDP socket_use_lowercase",
            build_ipv4_udp_socket_lower) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test of ipv6 UDP socket_use_lowercase",
            build_ipv6_udp_socket_lower) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test of ipv4 TCP socket_use_lowercase",
            build_ipv4_tcp_socket_lower) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test of ipv6 TCP socket_use_lowercase",
            build_ipv6_tcp_socket_lower) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Run tests to set up nonblocking sockets in TCP/UDP/IPV4/IPV6
    if (CU_add_test(pSuite, "test of ipv4 UDP socket_nonblocking",
            build_ipv4_udp_socket_nonblock) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test of ipv6 UDP socket_nonblocking",
            build_ipv6_udp_socket_nonblock) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test of ipv4 TCP socket_nonblocking",
            build_ipv4_tcp_socket_nonblock) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test of ipv6 TCP socket_nonblocking",
            build_ipv6_tcp_socket_nonblock) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Pass invalid values to family/proto
    if (CU_add_test(pSuite, "test passing a negative number as the\
            protocol (IPV4)", build_ipv4_socket_bad_proto_given) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test passing a string as the protocol (IPV4)",
            build_ipv4_socket_pass_string_as_proto) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test passing a negative number as the\
            protocol (IPV6)", build_ipv6_socket_bad_proto_given) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test passing a string as the protocol (IPV6)",
            build_ipv6_socket_pass_string_as_proto) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test passing a bad family ID to set up a UDP\
            socket", build_udp_socket_bad_family_given) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }
    if (CU_add_test(pSuite, "test passing a bad family ID to set up a TCP\
            socket", build_tcp_socket_bad_family_given) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test Send_to(IPV4)
    if (CU_add_test(pSuite, "test sending a test string over UDP socket (IPV4)",
        send_udp_dgram_ipv4) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test Send_to and Recv_from(IPV4)
    if (CU_add_test(pSuite, "test sending and receiving a test string over UDP socket (IPV4)",
        send_and_recv_udp_dgram_ipv4) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test Recv_from(IPV4)_nonblocking
    if (CU_add_test(pSuite, "test receiving a test string over UDP NONBLOCKING socket (IPV4)",
        recv_udp_dgram_ipv4_nonblock) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test Send_to(IPV6)
    if (CU_add_test(pSuite, "test sending a test string over UDP socket (IPV6)",
        send_udp_dgram_ipv6) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test Send_to and Recv_from(IPV6)
    if (CU_add_test(pSuite, "test sending and receiving a test string over UDP socket (IPV6)",
        send_and_recv_udp_dgram_ipv6) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test Send_to and Recv_from(IPV6)_nonblocking
    if (CU_add_test(pSuite, "test receiving a test string over UDP NONBLOCKING socket (IPV6)",
        recv_udp_dgram_ipv6_nonblock) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test Send and Recv(IPV4)
    if (CU_add_test(pSuite, "test sending and receiving a test string over TCP socket (IPV4)",
        send_and_recv_tcp_stream_ipv4) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test Send and Recv(IPV6)
    if (CU_add_test(pSuite, "test sending and receiving a test string over TCP socket (IPV6)",
        send_and_recv_tcp_stream_ipv6) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test Recv(IPV4)_nonblocking
    if (CU_add_test(pSuite, "test receiving a test string over TCP NONBLOCKING socket (IPV4)",
        recv_tcp_stream_ipv4_nonblock) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test Recv(IPV6)_nonblocking
    if (CU_add_test(pSuite, "test receiving a test string over TCP NONBLOCKING socket (IPV6)",
        recv_tcp_stream_ipv6_nonblock) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test MTU_limit_ipv4_TCP
    if (CU_add_test(pSuite, "test sending and receiving(ipv4)(TCP) a string > MTU size",
        test_MTU_limit_ipv4_TCP) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test MTU_limit_ipv6_TCP
    if (CU_add_test(pSuite, "test sending and receiving(ipv6)(TCP) a string > MTU size",
        test_MTU_limit_ipv6_TCP) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();

    }

    //Test MTU_limit_ipv6_UDP
    if (CU_add_test(pSuite, "test sending and receiving(ipv6)(UDP) a string > MTU size",
        test_MTU_limit_ipv6_UDP) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

    //Test MTU_limit_ipv4_UDP
    if (CU_add_test(pSuite, "test sending and receiving(ipv4)(UDP) a string > MTU size",
        test_MTU_limit_ipv4_UDP) == NULL)
    {
      CU_cleanup_registry();
      return CU_get_error();
    }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
