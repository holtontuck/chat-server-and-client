##User Guide


##What the net\_lib library is/does:
    net_lib is basically a set of wrappers for the C standard network library
apis. The library is used in almost exactly the same way as the C standard
network library functions are, with a few exceptions, noted below. The library contains the following functions:

**int Socket(int family, int proto, bool non\_block);**

The Socket() call accepts three required (and only three) input parameters:
**family**: an integer that is enumerated in the following way:
<ul> <li>ipv4 (or IPV4),
     <li>ipv6 (ir IPV6)
     <li>Note: Simply pass ipv4(IPV4) or ipv6(IPV6) as the first parameter. It is not
        a string, so do not use quotation marks around it; these are enumerated
        values.
</ul>
**proto**: an integer that is enumerated in the following way:
<ul><li>udp (or UDP),
    <li>tcp (or TCP)
    <li>Note: Simply pass udp(UDP) or tcp(TCP) as the second parameter, without
        quotation marks, as it is an enumerated value.
</ul>
**non\_block**: a boolean value; either True or False.
    <ul><li>Note: pass either True or False, without quotation marks. True
    indicates that the socket should be non-blocking. False indicates that the
    socket should block.
</ul>

Upon success, the Socket() call will return an integer socket descriptor. Upon
failure, the Socket() call will return -1 and set errno to the appropriate
error code.
<br>
<br>
<br>
<br>
**int Send(int sockfd, const void *buf, size_t len, int flags);**

The Send() call is used in conjunction with a TCP connection, after the
connection has been established via the standard TCP three-way handshake. It
has four required parameters:
<br>
<ul>
<li>sockfd: the socket descriptor for the socket through which you want to
pass data. This is usually the return value from the Socket() call, as assigned
to a variable. Example (int sock = Socket(ipv4, TCP, False) should return
a socket descriptor for a non-blocking, ipv4, TCP socket, and store the value
in the integer 'sock'.</li>
<li>buf: This is a void pointer to an area of memory where the data (that will
be passed to the socket) is stored. The data could be stored on the stack or
the heap, as the system call to send the data will actually send whatever data
is stored at the specified memory location, not a pointer to it. When a receive
call (Recv()) is used on the receiving side of the connection, the actual data
that has been passed over the socket will be placed into the receiver's
designated buffer.</li>
<li>len: the length, in bytes, of the buffer where the data is stored.</li>
<li>flags: this can be NULL. If you wish to specify flags to be passed, refer
to the man page for send().</li>
</ul>

A call to Send() would be executed after a socket has been created
and a connection has been made with the listening party (server). Once the
connection has been established, code for the Send() call may look like this:
(Assume the socket 'sock' was set up using the call in the example above)
<ul>
<li>size\_t buf\_size = 100;
<li>void \*buffer = calloc(1, buf\_size);
<li>Send(sock, buffer, buf\_size, NULL);
</ul>
Upon success, the Send() call will return an integer corresponding to the
number of bytes sent. Note: even if the other end of the connection does not
receive any bytes, or receives the wrong number of bytes, the Send() call is successful
if it is able to send the data out. Upon failure, the Send() call will return -1 
and set errno to the appropriate error code.
<br>
<br>
<br>
<br>
**int Send\_to(int sockfd, const void *buf, size_t len, int flags, const struct sockaddr *dest\_addr, socklen\_t addrlen);**

The Send\_to() call is used in conjunction with a UDP. Typically the
Send\_to() call is not used with a connected socket, so the call needs to specify
the address of the receiver.
<br>
<ul>
<li>sockfd: the socket descriptor for the socket through which you want to
pass data. This is usually the return value from the Socket() call, as assigned
to a variable. Example (int sock = Socket(ipv4, UDP, False) should return
a socket descriptor for a non-blocking, ipv4, UDP socket, and store the value
in the integer 'sock'.</li>
<li>buf: This is a void pointer to an area of memory where the data (that will
be passed to the socket) is stored. The data could be stored on the stack or
the heap, as the system call to send the data will actually send whatever data
is stored at the specified memory location, not a pointer to it. When a receive
call (Recv()) is used on the receiving side of the connection, the actual data
that has been passed over the socket will be placed into the receiver's
designated buffer.</li>
<li>len: the length, in bytes, of the buffer where the data is stored.</li>
<li>flags: this can be NULL. If you wish to specify flags to be passed, refer
to the man page for send().</li>
<li>dest\_addr: a pointer to a sockaddr struct which specifies the receiver's
ip address</li>
<li> addrlen: a positive integer of type socklen\_t, which specifies how many
bytes are in the receiver's ip address
</ul>

A call to Send\_to() would be executed after a socket has been created. Code for
the Send\_to() call may look like this:
(Assume the socket 'sock' was set up using the call in the example above)
<ul>
<li>size\_t buf\_size = 100;
<li>void \*buffer = calloc(1, buf\_size);
<li>Send\_to(sock, buffer, buf\_size, NULL, some\_socket\_addr, sizeof(some\_socket\_addr);
</ul>
Upon success, the Send\_to() call will return an integer corresponding to the
number of bytes sent. Note: even if the other end of the connection does not
receive any bytes, or recieves the wrong number of bytes, the Send\_to() call is successful if it is able to send the data out. Upon failure, the Send\_to() call will return -1 and set errno to the appropriate error code.
<br>
<br>
<br>
<br>
**int Recv(int sockfd, const void \*buf, size\_t len, int flags);**

The Recv() call is used in conjunction with a TCP connection, after the
connection has been established via the standard TCP three-way handshake. It
has four required parameters:
<br>
<ul>
<li>sockfd: the socket descriptor for the socket through which you want to
receive data. This is usually the return value from the Socket() call, as assigned
to a variable. Example (int sock = Socket(ipv4, TCP, False) should return
a socket descriptor for a non-blocking, ipv4, TCP socket, and store the value
in the integer 'sock'.</li>
<li>buf: This is a void pointer to an area of memory where the data (that will
be received from the socket) is stored.</li>
<li>len: the length, in bytes, of the buffer where the data is stored.</li>
<li>flags: this can be NULL. If you wish to specify flags to be passed, refer
to the man page for send().</li>
</ul>

A call to Recv() would be executed after a listening socket has been created
and a connection has been made with another party. Once the
connection has been established, code for the Recv() call may look like this:
(Assume the socket 'sock' was set up using the call in the example above)
<ul>
<li>size\_t buf\_size = 100;
<li>void \*buffer = calloc(1, buf\_size);
<li>Recv(sock, buffer, buf\_size, NULL);
</ul>
Upon success, the Recv() call will return an integer corresponding to the
number of bytes recevied. Upon failure, the Recv() call will return -1 
and set errno to the appropriate error code.
<br>
<br>
<br>
<br>
**int Recv\_from(int sockfd, const void *buf, size\_t len, int flags, const struct sockaddr *src\_addr, socklen\_t *addrlen);**

The Recv\_from() call is used in conjunction with a UDP socket. Typically the
Recv\_from() call is not used with a connected socket, so the call needs to specify
the address of the sender.
<ul>
<li>sockfd: the socket descriptor for the socket through which you want to
receive data. This is usually the return value from the Socket() call, as assigned
to a variable. Example (int sock = Socket(ipv4, UDP, False) should return
a socket descriptor for a non-blocking, ipv4, UDP socket, and store the value
in the integer 'sock'.</li>
<li>buf: This is a void pointer to an area of memory where the data (that will
be received from the socket) is stored.</li>
<li>len: the length, in bytes, of the buffer where the data is stored.</li>
<li>flags: this can be NULL. If you wish to specify flags to be passed, refer
to the man page for send().</li>
<li>src\_addr: a pointer to a sockaddr struct which specifies the sender's
ip address</li>
<li> addrlen: a pointer to positive integer of type socklen\_t, which specifies how many
bytes are in the sender's ip address
</ul>

A call to Recv\_from() would be executed after a socket has been created. Code for
the Recv\_from() call may look like this:
(Assume the socket 'sock' was set up using the call in the example above)
<ul>
<li>size\_t buf\_size = 100;
<li>void \*buffer = calloc(1, buf\_size);
<li>Recv\_from(sock, buffer, buf\_size, NULL, some\_socket\_addr, sizeof(some\_socket\_addr);
</ul>
Upon success, the Recv\_from() call will return an integer corresponding to the
number of bytes sent. Upon failure, the Recv\_from() call will return -1 
and set errno to the appropriate error code.
<br>
<br>
<br>
<br>
**void Close(int socket);**
The Close call accepts a socket descriptor, for an open socket, and attempts to
close the specified socket. There is no return value and no error checking is
performed.



NOTES ON DEPENDENCIES: There is a setup script that will attempt to check if
CUnit is installed on the target system. If not, the setup program will attempt
to install CUnit on the system. In order for 'make test' to execute, CUnit must
be installed on the target system. In addition, for 'make valgrind', the
program 'valgrind' must be installed on the target system. If you would like to
run 'make valgrind', make sure you install valgrind before attempting to run
'make valgrind'.

