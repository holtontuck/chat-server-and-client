# Netlib requirements

The purpose of this assignment is to create a network library and a echo server to test your network library. Additionally, you are responsible for providing all documentation and clearly identifying all project requirements for use with your library as well as a setup script to simplify the preparation of a Debian-based enviroment (preferably Ubuntu 18.04LTS) for use with your library.

## Conditions:
* Assume that the library will be compiled and run on a system running Ubuntu 18.04.
* Program will be written in only C.
* No third-party git interfaces, only use command-line git.
* No third-party networking libraries.
* No third-party encryption libraries.
* No direct commits to master branch.
* No memory leaks in Valgrind.
* No segmentation faults or other crashes.
* Support fragmented payloads larger than MTU.
* Incorporate the use of a setup file for installing dependencies.

## Library
* Implement UDP and TCP
* Establish socket
* Send
* Receive
* Close socket
* Implement encryption using binary operations(ROT13, XOR, etc.)
* IPv4
* Optional: IPv6

## Testing

Create an echo server that uses the netlib. 
- Packets sent to the echo server will be echoed back to the sender.

Observe test-driven development(TDD) throughout the development process.

Create a test report consisting of the following: 
 - Each test case and its result
 - Repeatable test cases
 - Screenshots of each test and its results
 - Verifies both pass and fail for test cases
 - Clear and concise steps to reproduce each test

## Documentation

User guide providing the following:
 - Usage of the library. 
 - Example code detailing usage
 - Details about special situations.
 - Considerations when building your library.
 
Design documentation consisting of the following:  
- Brief description of all functions to include in, out and purpose. 
- Function prototypes
- A breakdown of all structures and their data members with a brief description of its purpose.
- Generated using doxygen

## Build

Create a makefile with the following build targets:
* **all**: Creates all products.
* **netlib**: Compiles library.
* **setup**: Uses "sudo apt" to install dependencies.
* **test**: Compiles library and testing programs, runs test, creates testing documentation and deletes all products except the testing documentation.
* **doc**: Runs doxygen to create documentation.
* **clean**: Deletes all products.
